package criacao;

import java.awt.Image;
import java.awt.Toolkit;

public class Desenho {
	
	private Image image;
	private int x;
	private int y;

	public Image getImage() {
		return image;
	}

	public void setImage(String path) {

		this.image = Toolkit.getDefaultToolkit().createImage(path);
	}

	public int getX() {

		return x;
	}

	public int setX(int x) {

		return this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {

		this.y = y;
	}

}
