package criacao;

import excepion.CenarioUltrapassadoLadoDireitoException;
import excepion.CenarioUltrapassadoLadoEsquerdoException;
import imagens.CenarioInicial;
import imagens.CenarioInterno;
import imagens.Homer;

public class DesenhoMovel extends Desenho {

	private Homer homer;
	private CenarioInicial cenarioInicial;
	private CenarioInterno cenarioInterno;

	public void movimentoFrente(Homer homer) throws CenarioUltrapassadoLadoDireitoException {

		if ((homer.getX() >= 1140) || (homer.getX() >= 1060)) {

			throw new CenarioUltrapassadoLadoDireitoException(homer);

		}

		this.setX(getX() + 20);

	}

	public void movimentoTras(Homer homer) throws CenarioUltrapassadoLadoEsquerdoException {

		if ((homer.getX() <= -20)) {
			throw new CenarioUltrapassadoLadoEsquerdoException(homer);

		}

		this.setX(getX() - 20);

	}

}
