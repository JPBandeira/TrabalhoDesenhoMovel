package excepion;

import criacao.DesenhoMovel;

public class CenarioUltrapassadoLadoDireitoException extends Exception {

    private DesenhoMovel desenhoMovel;

    public CenarioUltrapassadoLadoDireitoException(DesenhoMovel desenhoMovel) {

        this.desenhoMovel = desenhoMovel;

    }

    public DesenhoMovel getCenarioUltrapassadoLadoDireitoException() {

        return desenhoMovel;

    }


}
