package excepion;

import criacao.DesenhoMovel;

public class CenarioUltrapassadoLadoEsquerdoException extends Exception {
	
	private DesenhoMovel desenhoMovel;
	
	public CenarioUltrapassadoLadoEsquerdoException(DesenhoMovel desenhoMovel) {

        this.desenhoMovel = desenhoMovel;

    }

    public DesenhoMovel getCenarioUltrapassadoLadoEsquerdoException() {

        return desenhoMovel;

    }

}
