package imagens;

import java.io.Serializable;

import criacao.Desenho;

public class CenarioInicial extends Desenho implements Serializable {


	public CenarioInicial() {
		
		super();
		
	}

	public CenarioInicial(int x, int y, String path) {
		
		super.setX(x);
		super.setY(y);
		super.setImage(path);
		
	}

}
