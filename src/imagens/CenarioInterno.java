package imagens;

import java.io.Serializable;

import criacao.Desenho;

public class CenarioInterno extends Desenho implements Serializable {

	public CenarioInterno() {
		
		super();
		
	}

	public CenarioInterno(int x, int y, String path) {
		
		super.setX(x);
		super.setY(y);
		super.setImage(path);
		
	}

}
