package thread;

import criacao.DesenhoMovel;
import imagens.CenarioInterno;
import principal.Jogo;

public class CenarioInternoThread extends DesenhoMovel implements Runnable {

	private Jogo jogo;

	public CenarioInternoThread(Jogo jogo) {


		this.jogo = jogo;


	}

	@Override
	public void run() {

		this.jogo.cenarioInicial.setX(20000000);
		this.jogo.cenarioInicial.setY(20000000);

		this.jogo.cenarioInterno = new CenarioInterno(0, 0, "moesbar.jpg");

	}

}
