package principal;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JFrame;

import criacao.InicioJogo;
import criacao.Jogador;
import excepion.CenarioUltrapassadoLadoDireitoException;
import excepion.CenarioUltrapassadoLadoEsquerdoException;
import imagens.CenarioInicial;
import imagens.CenarioInterno;
import imagens.Homer;
import thread.CenarioInternoThread;

public class Jogo extends JFrame implements KeyListener {

	File file = new File(
			"C:\\Users\\JP\\git\\TrabalhoDesenhoMovel\\arquivotrabalho.txt");

	private final CenarioInternoThread cenarioInternoThread;

	public CenarioInterno cenarioInterno;

	public Homer homer;
	public CenarioInicial cenarioInicial;

	Jogador jogador = new Jogador();
	InicioJogo inicioJogo = new InicioJogo();

	public static void main(String[] args) {

		Jogo jogo = new Jogo();

	}

	public Jogo() {

		jogador.setNome("Joao Pedro");
		inicioJogo.addJogador(jogador);

		cenarioInicial = new CenarioInicial(0, 0, "cenario.jpg");

		homer = new Homer(80, 440, "homer.gif");
		cenarioInterno = new CenarioInterno(200000, 200000, "moesbar.jpg");

		this.addKeyListener(this);
		this.setSize(800, 700);
		this.setVisible(true);
		this.setResizable(true);

		cenarioInternoThread = new CenarioInternoThread(this);

//		while (true) {
//			System.out.println("Posicao do X: " + homer.getX());
//		}
	}

	@Override
	public void keyPressed(KeyEvent keyEvent) {

		try {

			if (clicouSetaDireita(keyEvent)) {
				homer.movimentoFrente(homer);
				repaint();

			} else if (clicouSetaCima(keyEvent)) {
				if (homeEstaNaPorta()) {
					cenarioInternoThread.run();
					repaint();

				}
			} else if (clicouSetaEsquerda(keyEvent)) {
				homer.movimentoTras(homer);
				repaint();

			}

			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(inicioJogo);
			oos.close();
			fos.close();

		} catch (CenarioUltrapassadoLadoDireitoException cenarioLadoDireitoException) {

			System.out.println();
			System.out.println("**IMAGEM ULTRAPASSOU BORDA DIREITA DO CENARIO** "
					+ cenarioLadoDireitoException.getCenarioUltrapassadoLadoDireitoException().getX());

		} catch (CenarioUltrapassadoLadoEsquerdoException cenarioLadoEsquerdoException) {
			
			System.out.println();
			System.out.println("**IMAGEM ULTRAPASSOU BORDA ESQUERDA DO CENARIO** "
					+ cenarioLadoEsquerdoException.getCenarioUltrapassadoLadoEsquerdoException().getX());

		} catch (FileNotFoundException e) {

			System.out.println("Arquivo Nao Encontrado");

		} catch (IOException e) {

			System.out.println("Erro de E/S");

		}

		System.out.println();
		
		System.out.println("Tecla Apertada: " + keyEvent.getKeyChar() + " Codigo da tecla: " + keyEvent.getKeyCode() + " Posicao do Homer: " + homer.getX());

	}

	private boolean homeEstaNaPorta() {
		return (homer.getX() >= 480 && homer.getX() <= 620) && cenarioInicial.getX() == 0;
	}

	private boolean clicouSetaCima(KeyEvent keyEvent) {
		return keyEvent.getKeyCode() == KeyEvent.VK_UP;
	}

	private boolean clicouSetaDireita(KeyEvent e) {
		return e.getKeyCode() == KeyEvent.VK_RIGHT;
	}

	private boolean clicouSetaEsquerda(KeyEvent e) {
		return e.getKeyCode() == KeyEvent.VK_LEFT;
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	public void paint(Graphics graficos) {

		graficos.drawImage(homer.getImage(), homer.getX(), homer.getY(), this);
		graficos.drawImage(cenarioInicial.getImage(), cenarioInicial.getX(), cenarioInicial.getY(), null);
		graficos.drawImage(cenarioInterno.getImage(), cenarioInterno.getX(), cenarioInterno.getY(), null);

	}

}
